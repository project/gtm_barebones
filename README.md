# GTM Barebones

https://www.drupal.org/project/gtm_barebones

## Usage

This module uses a configuration-UI-less settings.

You can choose to export and modify config, or utilise config overrides in `settings.php`:

```php
$config['gtm_barebones.settings']['container_id'] = 'GTM-ABCDEFGHIJK';
$config['gtm_barebones.settings']['environment_id'] = 'env-123456';
$config['gtm_barebones.settings']['environment_token'] = 'iBN8NANliiuqnAAi81LapqkkdUIjak';
```

# CSP (Content Security Policy)

If your site enforces CSP, you must add configure CSP directives.

Consider using the [CSP module](https://www.drupal.org/project/csp)

Then add the directives in https://developers.google.com/tag-platform/security/guides/csp

Note the extra directives required for things like Preview mode, and Google Analytics integration.

## License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
